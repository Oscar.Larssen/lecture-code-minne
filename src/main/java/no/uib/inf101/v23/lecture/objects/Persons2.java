package no.uib.inf101.v23.lecture.objects;

import java.util.ArrayList;

public class Persons2 {

	public static void main(String[] args) {
		ArrayList<Person> persons = new ArrayList<Person>();
		fill(persons);
		printAll(persons);
		System.out.println();
		removeLast(persons);
		printAll(persons);
		System.out.println();
		persons.get(0).growOlder();
		printAll(persons);
	}

	private static void removeLast(ArrayList<Person> persons) {
		persons.remove(persons.size()-1);
	}

	/**
	 * Fills two lists with the same number of elements.
	 * names.get(i) and ages.get(i) are the name and age of person i
	 *  
	 * @param persons The List of persons to be filled
	 */
	public static void fill(ArrayList<Person> persons) {
		persons.add(new Person("Anna",12));
		persons.add(new Person("Per",3));
		persons.add(new Person("Hans",7));
		persons.add(new Person("Lise",9));
	}
	
	public static void printAll(ArrayList<Person> persons) {
		//for(int i=0; i<persons.size(); i++) {
		//	System.out.println(persons.get(i));
		//	}
		for(Person p : persons) {
			System.out.println(p);
		}
	}
}
